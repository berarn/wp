
<?php get_header();?>

<!-- Navbar Section Start -->
<div class="navbar-area">
      <!-- Menu For Mobile Device -->
      <div class="mobile-nav">
        <a href="index.html" class="logo">
          <img src="<?php bloginfo('template_directory');?>/assets/img/logo.png" alt="logo" />
        </a>
      </div>

      <!-- Menu For Desktop Device -->
      <div class="main-nav">
        <div class="container">
          <nav class="navbar navbar-expand-md navbar-light">
            <a class="navbar-brand" href="index.html">
              <img src="<?php bloginfo('template_directory');?>/assets/img/logo.png" alt="logo" />
            </a>
            <div
              class="collapse navbar-collapse mean-menu"
              id="navbarSupportedContent"
            >
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a href="#" class="nav-link active">
                    Pocetna
                  </a>
                </li>
                <li class="nav-item">
                  <a href="about.html" class="nav-link">O nama</a>
                </li>
                <li class="nav-item">
                  <a href="service.html" class="nav-link dropdown-toggle">
                    Usluge
                    <i class="icofont-rounded-right"></i>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="nav-item">
                      <a href="service.html" class="nav-link">Pranje prostora poslovnih objekata</a>
                    </li>
                    <li class="nav-item">
                      <a href="service.html" class="nav-link">Pranje stambenih objekata</a>
                    </li>
                    <li class="nav-item">
                      <a href="service-two.html" class="nav-link"
                        >Masinsko pranje tendi, nadstresnica i suncobrana</a
                      >
                    </li>
                    <li class="nav-item">
                      <a href="service-three.html" class="nav-link"
                        >Masinsko pranje podova</a
                      >
                    </li>
                    <li class="nav-item">
                      <a href="service-details.html" class="nav-link"
                        >Masinsko pranje pod visokim pritiskom</a
                      >
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a href="blog.html" class="nav-link">
                    Blog
                  </a>
                </li>
                
                </li>
                <li class="nav-item">
                  <a href="contact.html" class="nav-link">Kontakt</a>
                </li>
              </ul>
              <div class="navbar-button">
                <a href="testimonial.html">Estimacija</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <!-- Navbar Section End -->

    <!-- Home Section Start -->
    <div class="home-section">
      <div class="home-slider-area owl-carousel owl-theme">
        <div class="home-slider-item items-bg1">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="home-text">
                  <h1>Profesionalno pranje prozora, izloga tendi</h1>
                  <p>
                    Iskustvo koje smo sakupili tokom više od 30 godina uspešnog
                    poslovanja omogućava nam da svaki projekat koji stavite pred
                    nas završimo na najvišem nivou.
                  </p>

                  <div class="theme-button">
                    <a href="about.html" class="default-btn active-btn"
                      >Usluge</a
                    >
                    <a href="about.html" class="default-btn">Saznaj vise</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="home-slider-item items-bg2">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="home-text">
                  <h1>Profesionalno pranje prozora, izloga tendi</h1>
                  <p>
                    Iskustvo koje smo sakupili tokom više od 30 godina uspešnog
                    poslovanja omogućava nam da svaki projekat koji stavite pred
                    nas završimo na najvišem nivou.
                  </p>

                  <div class="theme-button">
                    <a href="about.html" class="default-btn active-btn"
                      >Usluge</a
                    >
                    <a href="about.html" class="default-btn">Saznaj vise</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="home-slider-item items-bg3">
          <div class="d-table">
            <div class="d-table-cell">
              <div class="container">
                <div class="home-text">
                  <h1>Our Working Process is Unique</h1>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor ut labore et dolore magna aliqua. Quis
                    ipsum suspendisse ultrices gravida. Viverra maecenas
                    accumsan lacus vel facilisis. Quis ipsum suspendisse
                    ultrices gravida.
                  </p>

                  <div class="theme-button">
                    <a href="about.html" class="default-btn active-btn"
                      >Usluge</a
                    >
                    <a href="about.html" class="default-btn">Saznaj vise</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Home Section End -->

    <!-- Recent Work Section Start -->
    <section class="recent-work-section pt-100">
      <div class="container-fluid p-0">
        <div class="section-head blue-title text-center">
          <h2>Nasi <span>zadovoljni klijenti</span></h2>
          <p>
            Saradjujemo sa našim klijentima na najvisem nivou već duže od 30
            godina . Medju njima su zastupljeni neki od vodecih svetskih
            brendova podjednako kao i manji lokalni biznisi.
          </p>
        </div>

        <div class="recent-work-slider owl-carousel owl-theme">
          <div class="recent-items">
            <div class="recent-img">
              <img src="<?php bloginfo('template_directory');?>/assets/img/clients/swarovski.png" alt="project image" />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/1.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img
                src="<?php bloginfo('template_directory');?>/assets/img/clients/volkswagen.png"
                alt="project image"
              />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/2.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img
                src="<?php bloginfo('template_directory');?>/assets/img/clients/calvin_klein.png"
                alt="project image"
              />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/3.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img
                src="<?php bloginfo('template_directory');?>/assets/img/clients/crowne_plaza.png"
                alt="project image"
              />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/4.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img src="<?php bloginfo('template_directory');?>/assets/img/clients/boutique.jpg" alt="project image" />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/5.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img src="<?php bloginfo('template_directory');?>/assets/img/clients/adiddas.png" alt="project image" />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/1.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img src="<?php bloginfo('template_directory');?>/assets/img/clients/lego.png" alt="project image" />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/2.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="recent-items">
            <div class="recent-img">
              <img src="<?php bloginfo('template_directory');?>/assets/img/clients/replay.png" alt="project image" />

              <div class="recent-hover">
                <div class="d-table">
                  <div class="d-table-cell">
                    <a
                      href="<?php bloginfo('template_directory');?>/assets/img/recent-work/3.png"
                      class="popup-gallery"
                    >
                      <i class="icofont-eye-alt"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Recent Work Section End -->

    <!-- Service Section Start -->
    <div class="service-style-two pt-100">
      <div class="container">
        <div class="section-head text-center">
          <h2><span>Usluge</span> koje pruzamo</h2>
        </div>

        <div class="service-slider-wrapper owl-carousel owl-theme">
          <div class="row align-items-center bg-white">
            <div class="col-lg-6">
              <div class="service-img">
                <img src="<?php bloginfo('template_directory');?>/assets/img/service/1.png" alt="service image" />
              </div>
            </div>

            <div class="col-lg-6">
              <div class="service-text-two">
                <h4>Pranje prozora poslovnih objekata</h4>
                <p>
                  Nije bitno da li je u pitanju lanac korporativnih lokala, velika poslovna zgrada, porodični lokal ili ugostitelji objekat profesionalni tim Super Šilja servisa će se pobrinuti da vaše klijente dočekaju blistavo čisti prozori.Naše usluge vršimo u fleksibilnim termima i po cenama koje će odgovarati svakom budžetu. 
                </p>

                <ul>
                  <li>
                    <i class="icofont-check-circled"></i>
                    Restorani
                  </li>
                  <li>
                    <i class="icofont-check-circled"></i>
                    Kafici
                  </li>
                  <li>
                    <i class="icofont-check-circled"></i>
                    Butici
                  </li>
                  <li>
                    <i class="icofont-check-circled"></i>
                    Maloprodajne objekte
                  </li>
                  <li>
                    <i class="icofont-check-circled"></i>
                    Banke
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Hotele
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Poslovne zgrade
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Zdravstvene ustanove
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Skladista
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Sedista firmi
                  </li>

                  <li>
                    <i class="icofont-check-circled"></i>
                    Kancelarijsti prostori
                  </li>
                </ul>

              </div>
            </div>
          </div>

          <div class="row align-items-center bg-white">
            <div class="col-lg-6">
              <div class="service-img">
                <img src="<?php bloginfo('template_directory');?>/assets/img/service/3.png" alt="service image" />
              </div>
            </div>

            <div class="col-lg-6">
              <div class="service-text-two">
                <h4>Masinsko pranje tendi, nadstresnica i suncobrana</h4>
                <p>
                  Restauracija i profesionalno ciscenje vaše stare tende obezbediće joj sjaj i svezinu kokvu zaslužuje vas objekat. 

                  Uštedite na kupovini novih tendi i suncobrana jer će vas i stare služiti podjednako dobro posle našeg tretmana. 

                  Naš profesionalni tim će detaljno očistiti svaku nečistoću od algi, budji, pricijeg izmeta, prašine koristeći profesionalni alat i sredstva za čišćenje. Koristimo najkvalitetnije biorazgradive preparate koji čuvaju tkanje i boju tende. 

                  Kada je vaša tenda potpuno čista nanosi o zaštitni sloj koji je štiti tkanje od dejstva UV zraka, oštećenja boje i talozenja prljavštine.
                </p>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Service Section End -->

    <!-- Why Choose Section Start -->
    <section class="why-choose-section why-choose-bg">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 why-choose-img"></div>
          <div class="col-lg-5 offset-lg-6 offset-md-0">
            <div class="why-choose-text">
              <div class="section-head">
                <h2>Zasto mi?</h2>
                <p>
                  Blistavi proozri predstavljaju vas biznis u najboljem svetlu
                </p>
              </div>
            </div>

            <div class="why-choose-accordian">
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <a
                        href="#"
                        class="btn"
                        data-toggle="collapse"
                        data-target="#collapseOne"
                        aria-expanded="true"
                        aria-controls="collapseOne"
                      >
                        Kvalitet
                      </a>
                    </h2>
                  </div>
                  <div
                    id="collapseOne"
                    class="collapse show"
                    aria-labelledby="headingOne"
                    data-parent="#accordionExample"
                  >
                    <div class="card-body">
                      Saradnja sa nama znaci da cete imati profesionalnu uslugu
                      koja garantuje najbolji moguci rezultat. Zbog nase
                      politike 100% zadovoljstva klijenata zavrsavamo svaki nas
                      projekat obilaskom odradjenog posla sa vama kako bi ste se
                      sami uverili u kvalitet.
                      <br />
                      Ukoliko niste zadovoljni besplatno cemo sanirati svaku
                      nepravilnost.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <a
                        href="#"
                        class="btn collapsed"
                        data-toggle="collapse"
                        data-target="#collapseTwo"
                        aria-expanded="false"
                        aria-controls="collapseTwo"
                      >
                        Iskustvo
                      </a>
                    </h2>
                  </div>
                  <div
                    id="collapseTwo"
                    class="collapse"
                    aria-labelledby="headingTwo"
                    data-parent="#accordionExample"
                  >
                    <div class="card-body">
                      Super Silja Servis već više od tri decenije pruža
                      prvoklasne usluge koje su nam obezbedile da izgradimo
                      vrhunsku reputaciju koja doprinosi da lista naših
                      zadovoljnih klijenata konstantno raste.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <a
                        href="#"
                        class="btn collapsed"
                        data-toggle="collapse"
                        data-target="#collapseThree"
                        aria-expanded="false"
                        aria-controls="collapseThree"
                      >
                        Povoljne usluge za svaki budzet
                      </a>
                    </h2>
                  </div>
                  <div
                    id="collapseThree"
                    class="collapse"
                    aria-labelledby="headingThree"
                    data-parent="#accordionExample"
                  >
                    <div class="card-body">
                      Nije bitno da li je u pitanju lanac korporativnih lokala,
                      velika poslovna zgrada, porodični lokal ili ugostitelji
                      objekat profesionalni tim Super Šilja servisa će se
                      pobrinuti da vaše klijente dočekaju blistavo čisti
                      prozori.Naše usluge vršimo u fleksibilnim termima i po
                      cenama koje će odgovarati svakom budžetu.
                    </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0">
                      <a
                        href="#"
                        class="btn collapsed"
                        data-toggle="collapse"
                        data-target="#collapseFour"
                        aria-expanded="false"
                        aria-controls="collapseFour"
                      >
                        Posebne pogodnosti i popusti
                      </a>
                    </h2>
                  </div>

                  <div
                    id="collapseFour"
                    class="collapse"
                    aria-labelledby="headingFour"
                    data-parent="#accordionExample"
                  >
                    <div class="card-body">
                      Preporucjujemo vam redovno odrzavanje vasih prozora jer
                      ćete na taj način uvek imati besprekorna stakla po
                      povoljnoj ceni . Politika naše firme je da se cena
                      pojedinačnog čišćenja smanjuje ukoliko odaberete dugoročnu
                      saradnju sa nama.
                    </div>
                  </div>
                </div>
              </div>

              <div class="why-choose-contact">
                <form>
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Vas br. telefona"
                    />
                  </div>
                  <button type="submit" class="btn btn-primary">
                    Zakazite poziv
                  </button>
                </form>
                <p>Besplatan poziv</p>
              </div>
            </div>
          </div>
        </div>

        <div class="why-choose-shape">
          <img src="<?php bloginfo('template_directory');?>/assets/img/why-choose/shape-1.png" alt="shape" />
        </div>
      </div>
    </section>
    <!-- Why Choose Section End -->

    <!-- Counter Section Start -->
    <div class="counter-section pt-100">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-5">
            <div class="offer-text">
              <h2>
                Nase usluge su povoljne i odgovarace <span>svakom budzetu</span>
              </h2>
              <div class="theme-button">
                <a href="contact.html" class="default-btn">Rezervisi</a>
              </div>
            </div>
          </div>

          <div class="col-lg-7">
            <div class="counter-area">
              <div class="row">
                <div class="col-lg-5 col-md-3 col-6 offset-lg-1">
                  <div class="counter-text">
                    <h2><span class="counter">10</span>000</h2>
                    <p>Obavljenih pranja</p>
                  </div>
                </div>

                <div class="col-lg-5 col-md-3 col-6">
                  <div class="counter-text">
                    <h2><span class="counter">200</span></h2>
                    <p>Srecnih klijenata</p>
                  </div>
                </div>

                <div class="col-lg-5 col-md-3 col-6 offset-lg-1">
                  <div class="counter-text">
                    <h2><span class="counter">45</span></h2>
                    <p>Strucnjaka</p>
                  </div>
                </div>

                <div class="col-lg-5 col-md-3 col-6">
                  <div class="counter-text">
                    <h2><span class="counter">35</span></h2>
                    <p>Godina iskustva</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="counter-shape">
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/1.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/2.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/3.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/4.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/5.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/6.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/7.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/1.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/8.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/4.png" alt="shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/shape-1.png" alt="bubble shape" />
          <img src="<?php bloginfo('template_directory');?>/assets/img/counter/shape-1.png" alt="bubble shape" />
        </div>
      </div>
    </div>
    <!-- Counter Section End -->

    <!-- Contact Section Start -->
    <div class="contact-section">
      <div class="container">
        <div class="contact-area">
          <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
              <div class="contact-img">
                <img src="<?php bloginfo('template_directory');?>/assets/img/contact-img.png" alt="contact image" />
              </div>
            </div>

            <div class="col-lg-6 col-md-6">
              <div class="contact-text">
                <div class="section-head">
                  <h2>Kontaktirajte<h2> <span>Super Silju</span></h2>
                  <p>Za sva pitanja kotaktirajte nas vec danas.</p>
                </div>
                <div class="contact-form">
                  <form id="contactForm">
                    <div class="row">
                      <div class="col-md-12 col-sm-6">
                        <div class="form-group">
                          <input
                            type="text"
                            name="name"
                            id="name"
                            class="form-control"
                            required
                            data-error="Unesite ime"
                            placeholder="Vase ime"
                          />
                          <div class="help-block with-errors"></div>
                        </div>
                      </div>

                      <div class="col-md-12 col-sm-6">
                        <div class="form-group">
                          <input
                            type="email"
                            name="email"
                            id="email"
                            class="form-control"
                            required
                            data-error="Unesite e-mail adresu"
                            placeholder="Vas e-mail"
                          />
                          <div class="help-block with-errors"></div>
                        </div>
                      </div>

                      <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                          <textarea
                            name="message"
                            class="form-control"
                            id="message"
                            cols="30"
                            rows="5"
                            required
                            data-error="Ostavite poruku"
                            placeholder="Vasa poruka"
                          ></textarea>
                          <div class="help-block with-errors"></div>
                        </div>
                      </div>

                      <div class="col-lg-12 col-md-12">
                        <button type="submit" class="default-btn page-btn">
                          Posaljite poruku - besplatno
                        </button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Contact Section End -->

    <?php get_footer();?>