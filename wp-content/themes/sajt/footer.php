     <!-- Footer Section Start -->
     <footer class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-6">
            <div class="footer-widget pl-40">
              <h3>Usluge</h3>
              <ul>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="service-details.html">Pranje prozora poslovnih objekata</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="service-details.html">Pranje prozora stambenih objekata</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="service-details.html">Masinsko pranje tendi, nadstresnica...</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="service-details.html">Masinsko pranje</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="service-details.html">Office Cleaning</a>
                </li>
              </ul>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="footer-widget pl-40">
              <h3>Brzi linkovi</h3>
              <ul>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="index.html">Pocetna</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="about.html">O Nama</a>
                </li>
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="blog.html">Blog</a>
                </li>
                
                <li>
                  <i class="icofont-simple-right"></i>
                  <a href="kontakt.html">Kontakt</a>
                </li>
              </ul>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="footer-widget">
              <h3>Pronadjite nas</h3>
              <p class="find-text">
                Locirani smo u Beogradu, naše usluge čišćenja i pranja pružamo u svakom delu grada , po potrebi dolazimo na lokacije širom Srbije. 
              </p>
              <ul class="find-us">
                <li>
                  <i class="icofont-location-pin"></i>
                  28/A Street, New York City
                </li>
                <li>
                  <i class="icofont-phone"></i>
                  <a href="tel:+880123456789">
                    +88 0123 456 789
                  </a>
                </li>
                <li>
                  <i class="icofont-ui-message"></i>
                  <a href="mailto:info@viscous.com">
                    info@viscous.com
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-footer">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="footer-social">
                <ul>
                  <li>
                    <a href="#"><i class="icofont-facebook"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="icofont-instagram"></i></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="copyright-text text-right">
                <p>
                  &copy;2020 Super Silja. Sva prava rezervisana
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Back to top -->
    <div class="top-btn">
      <i class="icofont-scroll-long-up"></i>
    </div>

    <?php wp_footer(); ?>
    </body>

    </html>