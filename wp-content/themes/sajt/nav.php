<!-- Navbar Section Start -->
<div class="navbar-area">
      <!-- Menu For Mobile Device -->
      <div class="mobile-nav">
        <a href="index.html" class="logo">
          <img src="<?php bloginfo('template_directory');?>/assets/img/logo.png" alt="logo" />
        </a>
      </div>

      <!-- Menu For Desktop Device -->
      <div class="main-nav">
        <div class="container">
          <nav class="navbar navbar-expand-md navbar-light">
            <a class="navbar-brand" href="index.html">
              <img src="<?php bloginfo('template_directory');?>/assets/img/logo.png" alt="logo" />
            </a>
            <div
              class="collapse navbar-collapse mean-menu"
              id="navbarSupportedContent"
            >
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a href="#" class="nav-link active">
                    Pocetna
                  </a>
                </li>
                <li class="nav-item">
                  <a href="about.html" class="nav-link">O nama</a>
                </li>
                <li class="nav-item">
                  <a href="service.html" class="nav-link dropdown-toggle">
                    Usluge
                    <i class="icofont-rounded-right"></i>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="nav-item">
                      <a href="service.html" class="nav-link">Pranje prostora poslovnih objekata</a>
                    </li>
                    <li class="nav-item">
                      <a href="service.html" class="nav-link">Pranje stambenih objekata</a>
                    </li>
                    <li class="nav-item">
                      <a href="service-two.html" class="nav-link"
                        >Masinsko pranje tendi, nadstresnica i suncobrana</a
                      >
                    </li>
                    <li class="nav-item">
                      <a href="service-three.html" class="nav-link"
                        >Masinsko pranje podova</a
                      >
                    </li>
                    <li class="nav-item">
                      <a href="service-details.html" class="nav-link"
                        >Masinsko pranje pod visokim pritiskom</a
                      >
                    </li>
                  </ul>
                </li>
                <li class="nav-item">
                  <a href="blog.html" class="nav-link">
                    Blog
                  </a>
                </li>
                
                </li>
                <li class="nav-item">
                  <a href="contact.html" class="nav-link">Kontakt</a>
                </li>
              </ul>
              <div class="navbar-button">
                <a href="testimonial.html">Estimacija</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
    <!-- Navbar Section End -->

    